#!/bin/bash

export BAMBOO_USER_HOME=/home/bamboo
export BAMBOO_AGENT_HOME=${BAMBOO_USER_HOME}/bamboo-agent-home

mkdir -p /home/bamboo/bamboo-agent-home/bin
#if [ -z ${1+x} ]; then
#    echo "Please run the Docker image with Bamboo URL as the first argument"
#    exit 1
#fi
if [ ! -f ${BAMBOO_CAPABILITIES} ]; then
   # cp ${INIT_BAMBOO_CAPABILITIES} ${BAMBOO_CAPABILITIES}
   cp /home/bamboo/init-bamboo-capabilities.properties /home/bamboo/bamboo-agent-home/bin/bamboo-capabilities.properties
fi

if [ -z ${SECURITY_TOKEN+x} ]; then   
    BAMBOO_SECURITY_TOKEN_PARAM=
else 
    BAMBOO_SECURITY_TOKEN_PARAM="-t ${SECURITY_TOKEN}"
fi 
export LC_ALL=C.UTF-8
export LANG=C.UTF-8
export LANGUAGE=C.UTF-8

java -Dbamboo.home=/home/bamboo/bamboo-agent-home -jar /home/bamboo/atlassian-bamboo-agent-installer.jar http://bamboo.apps.13.95.194.63.xip.io/agentServer/ ${BAMBOO_SECURITY_TOKEN_PARAM}
