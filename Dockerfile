FROM ubuntu:18.04
MAINTAINER Atlassian Bamboo Team

ARG BAMBOO_VERSION=6.10.3
ENV BAMBOO_USER=bamboo
ENV BAMBOO_GROUP=bamboo
ENV BAMBOO_USER_HOME=/home/${BAMBOO_USER}
ENV BAMBOO_JMS_CONNECTION_PORT=54663

ENV BAMBOO_SERVER_HOME          /var/atlassian/application-data/bamboo
ENV BAMBOO_SERVER_INSTALL_DIR   /opt/atlassian/bamboo

# Expose HTTP and AGENT JMS ports
EXPOSE 8085
EXPOSE $BAMBOO_JMS_CONNECTION_PORT

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl git bash procps openssl libtcnative-1 openjdk-8-jdk vim sudo iputils-ping wget tcpdump telnet
#install Maven separately to avoid JDK 10 installation
RUN apt-get install -y maven

RUN addgroup ${BAMBOO_GROUP} && \
     adduser ${BAMBOO_USER} --gecos "First Last,RoomNumber,WorkPhone,HomePhone" --home ${BAMBOO_USER_HOME} --ingroup ${BAMBOO_GROUP}
#Installing docker
RUN  apt-get -y install apt-transport-https  \
     gnupg2 \
     net-tools \
     systemd \
     software-properties-common
     
#RUN apt-get install \ btrfs-tools \ git \ golang-go \ go-md2man \ iptables \ libassuan-dev \ libc6-dev \ libdevmapper-dev \ libglib2.0-dev \ libgpgme-dev \ libgpg-error-dev \ libostree-dev \ libprotobuf-dev \ libprotobuf-c-dev \ libseccomp-dev \ libselinux1-dev \ libsystemd-dev \ pkg-config \ runc \ uidmap


RUN  apt-get update -qq && \
apt-get install -qq -y software-properties-common uidmap && \
add-apt-repository -y ppa:projectatomic/ppa && \
apt-get update -qq && \
apt-get -qq -y install buildah && \
apt-get -qq -y install podman


ARG DOWNLOAD_URL=https://www.atlassian.com/software/bamboo/downloads/binary/atlassian-bamboo-${BAMBOO_VERSION}.tar.gz

RUN mkdir -p ${BAMBOO_SERVER_INSTALL_DIR}/lib/native && \
    mkdir -p ${BAMBOO_SERVER_HOME} && \
    ln --symbolic "/usr/lib/x86_64-linux-gnu/libtcnative-1.so" "${BAMBOO_SERVER_INSTALL_DIR}/lib/native/libtcnative-1.so";

RUN curl -L --silent ${DOWNLOAD_URL} | tar -xz --strip-components=1 -C "$BAMBOO_SERVER_INSTALL_DIR"

RUN echo "bamboo.home=${BAMBOO_SERVER_HOME}" > $BAMBOO_SERVER_INSTALL_DIR/atlassian-bamboo/WEB-INF/classes/bamboo-init.properties

RUN chown -R "${BAMBOO_USER}:${BAMBOO_GROUP}" "${BAMBOO_SERVER_INSTALL_DIR}"
RUN chown -R "${BAMBOO_USER}:${BAMBOO_GROUP}" "${BAMBOO_SERVER_HOME}"
RUN chown -R bamboo:bamboo /opt/atlassian/bamboo/work/

RUN chmod -R 777 /opt/atlassian/bamboo/ && \
    chmod -R 777 /var/atlassian/application-data/bamboo/
    
COPY entrypoint.sh              /entrypoint.sh
RUN chown ${BAMBOO_USER}:${BAMBOO_GROUP} /entrypoint.sh
RUN chmod +x /entrypoint.sh
#create symlink to automate capability detection
RUN ln -s /usr/share/maven /usr/share/maven3

VOLUME ["${BAMBOO_SERVER_HOME}"]

USER ${BAMBOO_USER}
WORKDIR $BAMBOO_SERVER_HOME

ENTRYPOINT ["/entrypoint.sh"]